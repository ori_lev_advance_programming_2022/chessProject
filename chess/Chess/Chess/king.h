#pragma once
#include "Piece.h"
#include <vector>


class Position;
class Piece;
class Board;

class King : public Piece
{
public:
	//C'tor
	King(const char type);

	//Functions
	int isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const override;

	std::vector<Position*> getLegalMoves(const Position& src) const;
};