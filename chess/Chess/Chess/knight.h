#pragma once
#include "Piece.h"
#include "position.h"

class Piece;
class Position;

class Knight : public Piece
{
public:
	//C'tor
	Knight(const char type);

	//Functions
	int isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const override;
};