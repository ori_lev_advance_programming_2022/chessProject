#include "Piece.h"

#include "King.h"
#include "Queen.h"
#include "Bishop.h"
#include "Rook.h"
#include "Knight.h"
#include "Pawn.h"
#include "Empty.h"


Piece::Piece(const char& type)
{
	_type = type;
    _hasMoved = false;
}



char Piece::getType() const
{
	return _type;
}

bool Piece::getHasMoved()
{
    return _hasMoved;
}


void Piece::setHasMoved()
{
    // always true..
    _hasMoved = true;
}

int Piece::isBlack() const
{
    if (islower(_type))
        return BLACK;
    if (isupper(_type))
        return WHITE;
    if (_type == EMPTY)
        return EMPTY_TYPE;
    
    throw std::string("ERROR: Invalid type!");
}



int Piece::preLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
    // check if the index is out of the board
    if (!(src.legalPosition() && dest.legalPosition()))
        return INVALID_INDEX;

    // check if the tool dos not move
    if (src == dest)
        return SAME_POSITION;

    // check if the soure position have player, and the tool have the right color
    if ((int)currPlayer != board[src]->isBlack())
        return EMPTY_SOURCE;

    // check if the destination position have player, and the tool have the right color
    if ((int)currPlayer == board[dest]->isBlack())
        return FULL_DEST;
        
    
    return LEGAL_MOVE;
}


int Piece::isBlockedDiagonally(const Board& board, const Position& src, const Position& dest) const
{
    int i = 0, j = 0;
    Position p(0, 0);

    // check if the tool moves horizontaly
    if (abs((int)(dest.getX() - src.getX())) != abs((int)(dest.getY() - src.getY())))
        return ILLEGAL_MOVE;

    // down, right
    for (i = (int)src.getX() + 1, j = (int)src.getY() + 1; i < (int)dest.getX() && j < (int)dest.getY(); i++, j++)
    {
        p.setX(i);
        p.setY(j);

        if (board[p]->getType() != EMPTY)
            return ILLEGAL_MOVE;
    }

    // down, left
    for (i = (int)src.getX() + 1, j = (int)src.getY() - 1; i < (int)dest.getX() && j >(int)dest.getY(); i++, j--)
    {
        p.setX(i);
        p.setY(j);

        if (board[p]->getType() != EMPTY)
            return ILLEGAL_MOVE;
    }

    //up, right
    for (i = (int)src.getX() - 1, j = (int)src.getY() + 1; i > (int)dest.getX() && j < (int)dest.getY(); i--, j++)
    {
        p.setX(i);
        p.setY(j);

        if (board[p]->getType() != EMPTY)
            return ILLEGAL_MOVE;
    }

    //up, left
    for (i = (int)src.getX() - 1, j = (int)src.getY() - 1; i > (int)dest.getX() && j > (int)dest.getY(); i--, j--)
    {
        p.setX(i);
        p.setY(j);

        if (board[p]->getType() != EMPTY)
            return ILLEGAL_MOVE;
    }

    return LEGAL_MOVE;
}


int Piece::isBlockedHorizontaly(const Board& board, const Position& src, const Position& dest) const
{
    int i = 0;
    Position p(0, 0);
    int code = LEGAL_MOVE;

    // check if the tool moves horizontaly
    if (src.getX() != dest.getX() && src.getY() != dest.getY())
        return ILLEGAL_MOVE;
    
    if (dest.getX() != src.getX())
    {
        // check if he moved down.
        for (i = src.getX() + 1; i < (int)dest.getX(); i++)
        {
            // same y
            p.setX(i);
            p.setY(src.getY());
            
            if (board[p]->getType() != EMPTY)
                return ILLEGAL_MOVE;
        }

        // check if he moved up.
        for (i = src.getX() - 1; i > (int)dest.getX(); i--)
        {
            // same y
            p.setX(i);
            p.setY(src.getY());

            if (board[p]->getType() != EMPTY)
                return ILLEGAL_MOVE;
        }

    }

    if (dest.getY() != src.getY())
    {
        // check if he moved right.
        for (i = src.getY() + 1; i < (int)dest.getY(); i++)
        {
            // same x
            p.setX(src.getX());
            p.setY(i);

            if (board[p]->getType() != EMPTY)
                return ILLEGAL_MOVE;
        }

        // check if he moved left.
        for (i = src.getY() - 1; i > (int)dest.getY(); i--)
        {
            // same x
            p.setX(src.getX());
            p.setY(i);

            if (board[p]->getType() != EMPTY)
                return ILLEGAL_MOVE;
        }
    }

    return LEGAL_MOVE;
}


// function create a new piece* (can be any game tool)
Piece* Piece::getPiece(const char& type)
{

    switch (type)
    {
        
    case WHITE_KING:
    case BLACK_KING:
        return new King(type);
        break;

    case WHITE_QUEEN:
    case BLACK_QUEEN:
        return new Queen(type);
        break;

    case WHITE_BISHOP:
    case BLACK_BISHOP:
        return new Bishop(type);
        break;

    case WHITE_ROOK:
    case BLACK_ROOK:
        return new Rook(type);
        break;

    case WHITE_KNIGHT:
    case BLACK_KNIGHT:
        return new Knight(type);
        break;

    case WHITE_PAWN:
    case BLACK_PAWN:
        return new Pawn(type);
        break;

    case EMPTY:
        return new Empty(type);
        break;
        
    default:
        throw std::string("Invalid string");
        return nullptr;
    }
}


std::vector<Position*> Piece::getTrack(const Board& board, const Position& src, const Position& dest) const
{
    std::vector<Position*> temp;
    return temp;
}

void Piece::freeTrack(std::vector<Position*> track)
{
    int i = 0;
    for (i = 0; i < track.size(); i++)
        delete track[i];
}
