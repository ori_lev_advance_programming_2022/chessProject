#include "board.h"
#include "Pipe.h"
#include "piece.h"


class Game
{
public:
	Game();
	~Game();

	void play();
	void changeTurn();

private:
	bool _turn;
	Board* _board;
	Pipe _pipe;
};