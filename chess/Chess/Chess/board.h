#pragma once
#ifndef BOARD_H
#define BOARD_H

#include <string>

#include "Piece.h"

class Piece;


class Board
{
public:
	//C'tor
	Board(const std::string& board);
	//D'tor
	~Board();
	
	//getters
	std::string toString();

	//operators
	Piece* operator[](Position pos) const;

	//functions
	void move(Position src, Position dest);
	void setPosition(Piece* piece, Position dest);
	Position findKing(bool currPlayer);
	bool isCheck(bool currPlayer, Position* attacker);
	bool isMate(bool currPlayer);
	
private:
	Piece** _board[BOARD_HEIGHT]; // array of array's of Piece*
};

#endif // !BOARD_H