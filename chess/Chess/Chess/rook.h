#pragma once
#include "Piece.h"
#include "position.h"

class Piece;
class Position;

class Rook : public Piece
{
public:
	//C'tor
	Rook(const char type);

	//Functions
	int isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const override;
	std::vector<Position*> getTrack(const Board& board, const Position& src, const Position& dest) const override;
};