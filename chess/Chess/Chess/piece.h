#pragma once
#ifndef PIECE_H
#define PIECE_h

#include "Position.h"
#include "Board.h"
#include <vector>


class Board;
class Position;

class King;
class Queen;
class Bishop;
class Rook;
class Knight;
class Pawn;
class Empty;


enum whiteTypes { WHITE_KING = 'K', WHITE_QUEEN = 'Q', WHITE_ROOK = 'R', WHITE_KNIGHT = 'N', WHITE_BISHOP = 'B', WHITE_PAWN = 'P', EMPTY = '#' };
enum blackTypes { BLACK_KING = 'k', BLACK_QUEEN = 'q', BLACK_ROOK = 'r', BLACK_KNIGHT = 'n', BLACK_BISHOP = 'b', BLACK_PAWN = 'p' };

enum codeStatus { LEGAL_MOVE = 0, CHECK_MOVE, EMPTY_SOURCE, FULL_DEST, SELF_CHECK, INVALID_INDEX, ILLEGAL_MOVE, SAME_POSITION, MATT };
enum pieceType { WHITE = 0, BLACK, EMPTY_TYPE };

//sqrt(8^2 * 8^2)
#define MAXSTEPS 12


class Piece 
{
public:
	//C'tor
	Piece(const char& type);
	//D'tor
	virtual ~Piece() {};

	//getters
	char getType() const;
	bool getHasMoved();

	//setters
	void setHasMoved();

	int isBlack() const;

	virtual int isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const = 0;

	int preLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const;

	int isBlockedDiagonally(const Board& board, const Position& src, const Position& dest) const;
	int isBlockedHorizontaly(const Board& board, const Position& src, const Position& dest) const;

	static Piece* getPiece(const char& type);

	virtual std::vector<Position*> getTrack(const Board& board, const Position& src, const Position& dest) const;
	static void freeTrack(std::vector<Position*> track);
	

protected:
	char _type;
	bool _hasMoved;
};
#endif //!PIECE_h