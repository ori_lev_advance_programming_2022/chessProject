#include "board.h"

#include "king.h"
#define END_OF_LINE '\n'


Board::Board(const std::string& board)
{
	int i = 0, j = 0;
	char curr = ' ';

	// initilaze array of array's of Piece*
	for (i = 0; i < BOARD_HEIGHT; i++)
		_board[i] = new Piece * [BOARD_WIDTH];

	// put Piece* in each cell
	for (i = 0; i < BOARD_HEIGHT; i++)
	{
		for (j = 0; j < BOARD_WIDTH; j++)
		{
			curr = board[i * (int)BOARD_HEIGHT + j];

			_board[i][j] = Piece::getPiece(curr);
		}
	}
}


Board::~Board()
{
	int i = 0, j = 0;
	for (i = 0; i < BOARD_HEIGHT; i++)
		for (j = 0; j < BOARD_WIDTH; j++)
			delete _board[i][j];

	for (i = 0; i < BOARD_HEIGHT; i++)
		delete[] _board[i];
}


std::string Board::toString()
{
	unsigned int x = 0, y = 0;
	std::string res = "";

	for (x = 0; x < BOARD_HEIGHT; x++)
	{
		for (y = 0; y < BOARD_WIDTH; y++)
		{
			res += (*this)[Position(x, y)]->getType();
		}
		res += END_OF_LINE;
	}

	return res;
}


Piece* Board::operator[](Position pos) const
{
	if (!pos.legalPosition())
		throw std::string("Invalid position - out of bound.");

	return _board[pos.getX()][pos.getY()];
}


void Board::move(Position src, Position dest)
{
	delete (*this)[dest]; //delete the piece in the dest (prevente memory leaks)

	_board[dest.getX()][dest.getY()] = (*this)[src]; // put in the dest the src

	_board[src.getX()][src.getY()] = Piece::getPiece(EMPTY); // put in the src empty piece
}


void Board::setPosition(Piece* piece, Position dest)
{
	delete (*this)[dest];
	_board[dest.getX()][dest.getY()] = piece;
}


bool Board::isCheck(bool currPlayer, Position* attacker)
{
	unsigned int i = 0, j = 0;
	int attackerType = (int)currPlayer == WHITE ? BLACK : WHITE; // get the attacker type
	Position king(0, 0), temp(0, 0);
	bool flag = true;

	king = findKing(currPlayer); // get the king to attack position

	// loop throght the board and check for any attacker piece if he can kill the king.
	for (i = 0; i < BOARD_HEIGHT; i++)
	{
		for (j = 0; j < BOARD_WIDTH; j++)
		{
			temp.setX(i);
			temp.setY(j);

			if ((*this)[temp]->isLegal(*this, temp, king, (*this)[temp]->isBlack()) == LEGAL_MOVE)
			{
				attacker->setX(i);
				attacker->setY(j);

				return true;
			}
		}
	}

	return false;
}


Position Board::findKing(bool currPlayer)
{
	int i = 0, j = 0;
	char toFindType = (int)currPlayer == WHITE ? BLACK_KING : WHITE_KING;

	for (i = 0; i < BOARD_HEIGHT; i++)
	{
		for (j = 0; j < BOARD_WIDTH; j++)
		{
			if ((*this)[Position(i, j)]->getType() == toFindType)
				return Position(i, j);
		}
	}

	throw std::string("No king found!");

	return Position(0, 0);		//just to trick the compiler
}

// not implemented yet!
bool Board::isMate(bool currPlayer)
{
	int i = 0, j = 0, k = 0, counter = 0;
	Position king = findKing(!currPlayer), temp(king);
	Position attacker(0, 0), trackPosition(0, 0), p(0, 0);
	int hasMoved = 0;
	bool flag = false;
	char type = ' ';
	std::vector<Position*> track, kingMoves;
	
	//initializing king attacker
	if (!isCheck(!currPlayer, &attacker))
		return false;

	
	kingMoves = ((King*)((*this)[king]))->getLegalMoves(king);

	for (k = 0; k < kingMoves.size(); k++)
	{
		temp = *(kingMoves[k]);
		//if he can move we make sure the movement will accure another check
		if ((*this)[king]->isLegal(*this, king, temp, currPlayer) == LEGAL_MOVE)
		{
			hasMoved = (*this)[temp]->getHasMoved();
			type = (*this)[temp]->getType();

			this->move(king, temp);

			if (!isCheck(currPlayer, &attacker))
				flag = true;

			this->move(temp, king);
			this->setPosition(Piece::getPiece(type), temp);
		}
	}
	
	Piece::freeTrack(kingMoves);
	if (flag)
		return false;

	flag = false;
	
	track = (*this)[attacker]->getTrack(*this, attacker, king);

	for (k = 0; k < track.size(); k++)
	{
		trackPosition = Position(*(track[k]));

		for (i = 0; i < BOARD_HEIGHT; i++)
		{
			for (j = 0; j < BOARD_WIDTH; j++)
			{
				temp.setX(i);
				temp.setY(j);

				if ((*this)[temp]->isLegal(*this, temp, trackPosition, (*this)[temp]->isBlack()))
				{
					hasMoved = (*this)[temp]->getHasMoved();
					type = (*this)[temp]->getType();
					this->move(trackPosition, temp);


					if (!isCheck(currPlayer, &p))
					{
						Piece::freeTrack(track);
						return true;
					}

					this->move(temp, trackPosition);
					this->setPosition(Piece::getPiece(type), temp);
				}
			}
		}
	}

	Piece::freeTrack(track);
	return false;
}


