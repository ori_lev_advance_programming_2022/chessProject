#include "pawn.h"

#define NO_DIFFERENCE 0
#define NORMAL_DIFFERENCE 1
#define OPENNING_DIFFERENCE 2

Pawn::Pawn(const char type) : Piece(type)
{
	
}


int Pawn::isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
	int code = preLegal(board, src, dest, currPlayer);
	int xDifference = src.getX() - dest.getX();
	int yDifference = abs((int)src.getY() - (int)dest.getY());

	if (code != LEGAL_MOVE)
		return code;
	
	// check if the pawn move up/dawn and shift him if needed
	// (pawn can not move back...)
	if (!isBlack())
		xDifference *= -1;
	

	// openning move (2 forward)
	if (xDifference == OPENNING_DIFFERENCE && yDifference == NO_DIFFERENCE && !board[src]->getHasMoved() && board[dest]->getType() == EMPTY)
	{
		code = isBlockedHorizontaly(board, src, dest);

		if (code != LEGAL_MOVE)
			return code;

		return LEGAL_MOVE;
	}

	// move one forward
	if (xDifference == NORMAL_DIFFERENCE && yDifference == NO_DIFFERENCE && board[dest]->getType() == EMPTY)
		return LEGAL_MOVE;
	
	// attack enemy
	if (xDifference == NORMAL_DIFFERENCE && yDifference == NORMAL_DIFFERENCE && board[dest]->getType() != EMPTY)
		return LEGAL_MOVE;
	
	return ILLEGAL_MOVE;
}