#include "rook.h"


Rook::Rook(const char type) : Piece(type)
{
	
}


int Rook::isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
	int code = preLegal(board, src, dest, currPlayer);

	if (code == LEGAL_MOVE)
		code = isBlockedHorizontaly(board, src, dest);

	return code;
}

std::vector<Position*> Rook::getTrack(const Board& board, const Position& src, const Position& dest) const
{
	int i = 0;
	std::vector<Position*> temp;
	Position* p = nullptr;

	if (src.getX() > dest.getX())
	{
		for (i = src.getX();i > dest.getX(); i--)
		{
			p = new Position(i, src.getY());
			temp.push_back(p);
		}
	}

	if (src.getX() < dest.getX())
	{
		for (i = src.getX(); i < dest.getX(); i++)
		{
			p = new Position(i, src.getY());
			temp.push_back(p);
		}
	}


	if (src.getY() > dest.getY())
	{
		for (i = src.getY(); i > dest.getY(); i--)
		{
			p = new Position(src.getX(), i);
			temp.push_back(p);
		}
	}

	if (src.getY() < dest.getY())
	{
		for (i = src.getY(); i < dest.getY(); i++)
		{
			p = new Position(src.getX(), i);
			temp.push_back(p);
		}
	}

	return temp;
}
