#include "queen.h"


Queen::Queen(const char type) : Piece(type)
{

}


int Queen::isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
	int code = preLegal(board, src, dest, currPlayer);

	if (code == LEGAL_MOVE)
	{
		code = isBlockedHorizontaly(board, src, dest);
		if (code != LEGAL_MOVE)
			code = isBlockedDiagonally(board, src, dest);
	}	
	
	return code;
}

std::vector<Position*> Queen::getTrack(const Board& board, const Position& src, const Position& dest) const
{
	int i = 0, j = 0;
	std::vector<Position*> temp;
	Position* p = nullptr;

	if (src.getX() > dest.getX() && src.getY() > dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i > dest.getX() && j > dest.getY(); i--, j--)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}

	if (src.getX() > dest.getX() && src.getY() < dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i > dest.getX() && j < dest.getY(); i--, j++)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}


	if (src.getX() < dest.getX() && src.getY() > dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i < dest.getX() && j > dest.getY(); i++, j--)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}

	if (src.getX() < dest.getX() && src.getY() < dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i < dest.getX() && j < dest.getY(); i++, j++)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}
	

	if (src.getX() > dest.getX() && src.getY() == dest.getY())
	{
		for (i = src.getX(); i > dest.getX(); i--)
		{
			p = new Position(i, src.getY());
			temp.push_back(p);
		}
	}

	if (src.getX() < dest.getX() && src.getY() == dest.getY())
	{
		for (i = src.getX(); i < dest.getX(); i++)
		{
			p = new Position(i, src.getY());
			temp.push_back(p);
		}
	}


	if (src.getY() > dest.getY() && src.getX() == dest.getX())
	{
		for (i = src.getY(); i > dest.getY(); i--)
		{
			p = new Position(src.getX(), i);
			temp.push_back(p);
		}
	}

	if (src.getY() < dest.getY() && src.getX() == dest.getX())
	{
		for (i = src.getY(); i < dest.getY(); i++)
		{
			p = new Position(src.getX(), i);
			temp.push_back(p);
		}
	}
	return temp;
}

