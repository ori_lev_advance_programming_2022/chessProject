#include "Position.h"

//C'tor
Position::Position(const std::string& pos)
{
	if (pos.length() != POS_LENGTH || (!isalpha(pos[0]) && islower(pos[0])) || !isdigit(pos[1]))
		throw(std::string("Invalid position string"));
	// need to swap x & y (string reversed)
	setX(pos[1] - CHAR_DIGIT_TO_INT);
	setY(pos[0] - CHAR_TO_INT);
}


Position::Position(const int x, const int y)
{
	//no setter because we want it to be negative sometimes
	_x = x;
	_y = y;
}


Position::Position(const Position& other)
{
	setX(other.getX());
	setY(other.getY());
}


//getters
int Position::getX() const
{
	return _x;
}


int Position::getY() const 
{
	return _y;
}


//setters
void Position::setX(const int x)
{
	if (x < 0 || x >= BOARD_WIDTH)
		throw std::string("Invalid position - out of bound");

	 _x = x;
}


void Position::setY(const int y)
{
	if (y < 0 || y >= BOARD_HEIGHT)
		throw std::string("Invalid position - out of bound");

	_y = y;
}


bool Position::operator==(const Position& other) const 
{
	return _x == other.getX() && _y == other.getY();
}


Position& Position::operator=(const Position& other)
{
	setX(other.getX());
	setY(other.getY());
	
	return *this;
}


Position& Position::operator+=(const Position& other)
{
	setX(other.getX() + this->getX());
	setY(other.getY() + this->getY());

	return *this;
}


bool Position::legalPosition() const
{
	return (_x < BOARD_HEIGHT && _x >= 0) && (_y >= 0 && _y < BOARD_WIDTH);
}
