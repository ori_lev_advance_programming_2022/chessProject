#include "knight.h"

#include <iostream>
#include <math.h>

#define DIFF_ONE 1
#define DIFF_TWO 2


Knight::Knight(const char type) : Piece(type)
{

}


int Knight::isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
	int code = preLegal(board, src, dest, currPlayer);
	unsigned int xDifference = 0, yDifference = 0;

	if (code != LEGAL_MOVE)
		return code;

	// check if the knight move two steps forward and one left/right
	xDifference = abs((int)(src.getX() - dest.getX()));
	yDifference = abs((int)(src.getY() - dest.getY()));

	if ((xDifference == DIFF_ONE && yDifference == DIFF_TWO) || (xDifference == DIFF_TWO && yDifference == DIFF_ONE))
		return LEGAL_MOVE;

	return ILLEGAL_MOVE;
}
