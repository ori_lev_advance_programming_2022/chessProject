#include "bishop.h"

Bishop::Bishop(const char type) : Piece(type)
{

}


int Bishop::isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
	int code = preLegal(board, src, dest, currPlayer);

	if (code == LEGAL_MOVE)
		code = isBlockedDiagonally(board, src, dest);

	return code;
}

std::vector<Position*> Bishop::getTrack(const Board& board, const Position& src, const Position& dest) const
{
	int i = 0, j = 0;
	std::vector<Position*> temp;
	Position* p = nullptr;

	if (src.getX() > dest.getX() && src.getY() > dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i > dest.getX() && j > dest.getY(); i--, j--)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}

	if (src.getX() > dest.getX() && src.getY() < dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i > dest.getX() && j < dest.getY(); i--, j++)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}


	if (src.getX() < dest.getX() && src.getY() > dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i < dest.getX() && j > dest.getY(); i++, j--)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}

	if (src.getX() < dest.getX() && src.getY() < dest.getY())
	{
		for (i = src.getX(), j = src.getY(); i < dest.getX() && j < dest.getY(); i++, j++)
		{
			p = new Position(i, j);
			temp.push_back(p);
		}
	}

	return temp;
}


