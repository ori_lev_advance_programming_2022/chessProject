#include "game.h"
#include <crtdbg.h>

int main()
{
	// added the scope to call the game dtor
	{
		Game game;

		std::cout << "In the memory of Donkey and getMishbetzet()" << std::endl;

		game.play();
	}
	//delete game;

	std::cout << "Leaks: " << _CrtDumpMemoryLeaks() << std::endl;

	return 0;
}
