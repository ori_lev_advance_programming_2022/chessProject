#include "king.h"

#define NO_DIFFERENCE 0
#define MAX_DIFFERENCE 1
#define CASTLING_DIFFERENCE 2


King::King(const char type) : Piece(type)
{

}


int King::isLegal(const Board& board, const Position& src, const Position& dest, bool currPlayer) const
{
	int code = preLegal(board, src, dest, currPlayer);
	int xDifference = abs((int)(src.getX() - dest.getX()));
	int yDifference = abs((int)(src.getY() - dest.getY()));

	if (code != LEGAL_MOVE)
		return code;

	// no casteling yet.

	if (xDifference > MAX_DIFFERENCE || yDifference > MAX_DIFFERENCE)
		code = ILLEGAL_MOVE;

	return code;
}

std::vector<Position*> King::getLegalMoves(const Position& src) const
{
	int i = 0, j = 0;
	std::vector<Position*> moves;
	Position* temp;

	//Checking if the king can move
	for (i = -1; i <= 1; i++)
	{
		for (j = -1; j <= 1; j++)
		{
			temp = new Position(src.getX() + i, src.getY() + j);

			if (temp->legalPosition() && !(temp->getX() == src.getX() && temp->getY() == src.getY()))
				moves.push_back(temp);
			else
				delete temp;
		}
	}

	return moves;
}



