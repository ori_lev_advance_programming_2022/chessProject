#include "game.h"

#include <iostream>
#include <string>
#include <thread>


#define STARTING_COLOR WHITE
#define STARTING_BOARD_STRING "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"
#define STARTING_BOARD_FOR_BOARD "RNBKQBNRPPPPPPPP################################pppppppprnbkqbnr0"

Game::Game()
{
	_turn = STARTING_COLOR;
	_board = new Board(STARTING_BOARD_FOR_BOARD);
}

Game::~Game()
{
	delete _board;
}


void Game::play()
{
	unsigned int code = 0;
	bool isConnect = _pipe.connect();
	char msgToGraphics[1024];
	std::string ans;
	std::string msgFromGraphics = "";
	Position* src, *dest;
	Position attacker(0, 0);

	bool hasMMovedCopy = false;
	char typeCopy = ' ';
	Piece* p = nullptr;

	srand(time_t(NULL));

	while (!isConnect)
	{
		std::cout << "cant connect to graphics" << std::endl;
		std::cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << std::endl;
		std::cin >> ans;

		if (ans == "0")
		{
			std::cout << "trying connect again.." << std::endl;
			Sleep(5000);
			isConnect = _pipe.connect();
		}
		else
		{
			_pipe.close();
			return;
		}
	}


	
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, STARTING_BOARD_STRING); // just example...

	_pipe.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	msgFromGraphics = _pipe.getMessageFromGraphics();
	
	while (msgFromGraphics != "quit")
	{
		// handles the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		// create 2 position by the frontend message
		src = new Position(msgFromGraphics.substr(0, 2));
		dest = new Position(msgFromGraphics.substr(2, 2));

		// get the code of this move
		code = (*_board)[*src]->isLegal(*_board, *src, *dest, _turn);

		// put in message to frontend legal move (may chane letter)
		msgToGraphics[0] = LEGAL_MOVE + '0';
		msgToGraphics[1] = 0;

		
		if (code != LEGAL_MOVE)
			msgToGraphics[0] = code + '0'; // put the error message

		else
		{
			typeCopy = (*_board)[*dest]->getType(); // copied the piece who will may delete (move)
			hasMMovedCopy = (*_board)[*dest]->getHasMoved();

			_board->move(*src, *dest); // move the piece to the dest (to check if there are check)
			(*_board)[*dest]->setHasMoved();


			if (_board->isCheck(!_turn, &attacker))
			{
				// need to restore the board (move illegal (check)
				p =  Piece::getPiece(typeCopy); // create the piece

				if (hasMMovedCopy) // set his HasMoved if needed
					p->setHasMoved();
				
				_board->move(*dest, *src); // move the tool to his old place
				_board->setPosition(p, *dest); // put the eaten tool in his dest

				p = nullptr; // change p to nullptr (don't want to touch the piece anymore)

				msgToGraphics[0] = SELF_CHECK + '0';

				changeTurn();
			}
			
			else if(_board->isCheck(_turn, &attacker))
			{
				msgToGraphics[0] = CHECK_MOVE + '0';
				
				if(_board->isMate(!_turn))
					msgToGraphics[0] = MATT + '0';
			}
			
			changeTurn();
		}
		
		delete src;
		delete dest;

		// return result to graphics		
		_pipe.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = _pipe.getMessageFromGraphics();
	}
	
	_pipe.close();
}



void Game::changeTurn()
{
	_turn = !_turn;
}
