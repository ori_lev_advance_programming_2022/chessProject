#pragma once
#ifndef POSITION_H
#define POSITION_H

#include <iostream>

#define POS_LENGTH 2
#define BOARD_WIDTH 8
#define BOARD_HEIGHT 8

#define CHAR_TO_INT 'a'
#define CHAR_DIGIT_TO_INT '0' - 1


class Position
{
public:
	//C'tor
	Position(const std::string& pos);
	Position(const int x, const int y);
	Position(const Position& other);
	
	//D'tor
	~Position() {};
	
	//getters
	int getX() const;
	int getY() const;
	
	//setters
	void setX(const int x);
	void setY(const int y);

	//operators
	bool operator==(const Position& other) const;
	Position& operator=(const Position& other);
	Position& operator+=(const Position& other);

	// functions
	bool legalPosition() const;
private:
	int _x;
	int _y;
};

#endif // !POSITION_H